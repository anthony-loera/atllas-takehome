import { StatusBar } from "expo-status-bar";
import { StyleSheet, View } from "react-native";
import {
  NativeStackNavigationProp,
  NativeStackScreenProps,
} from "@react-navigation/native-stack";
import { StackScreens } from "../../App";
import { TextInput, Button } from "react-native-paper";
import React, { useState } from "react";
import axios, { AxiosError } from "axios";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function Login({}: NativeStackScreenProps<
  StackScreens,
  "Login"
>) {
  const [showPassword, setShowPassword] = useState(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const navigation = useNavigation<NativeStackNavigationProp<StackScreens>>();

  const toggleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleSubmit = async () => {
    try {
      const results = await axios.post(
        `${process.env.EXPO_PUBLIC_API_ROOT}/auth/login`,
        {
          username: username,
          password: password,
        }
      );
      const status = results.data.success;
      if (status === true) {
        navigation.navigate("Profile");
        await AsyncStorage.setItem("my-key", username);
      }
    } catch (error) {
      const e = error as AxiosError;
      console.log(e);
    }
  };

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <TextInput
        label="Username"
        id="Username"
        onChangeText={(text) => setUsername(text)}
      />
      <TextInput
        autoComplete="off"
        secureTextEntry={!showPassword}
        label="Password"
        id="Password"
        onChangeText={(text) => setPassword(text)}
      />
      <MaterialCommunityIcons
        name={showPassword ? "eye-off" : "eye"}
        size={24}
        color="#aaa"
        style={styles.icon}
        onPress={toggleShowPassword}
      />
      <Button mode="contained" onPress={handleSubmit}>
        Login
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  icon: {
    marginLeft: 10,
  },
});
