import Card from "@mui/material/Card";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { useState } from "react";
import axios from "axios";
import { useRouter } from "next/navigation";

interface User {
  username: String;
  password: String;
}

export default function SignUp() {
  const [values, setValues] = useState<User>();
  const router = useRouter();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    const name = e.target.name;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleSubmit = async () => {
    const results = await axios.post("http://localhost:50000/auth/register", {
      username: values.username,
      password: values.password,
    });
    alert(results.data.message);
    router.push("/signIn");
  };

  return (
    <Card
      sx={{
        bgcolor: "transparent",
        width: "40vh",
        height: "25vh",
        display: "flex",
        overflow: "none",
        flexDirection: "column",
        justifyContent: "space-between",
        padding: "4vh",
      }}
    >
      <div>Hello Sign up Page</div>
      <TextField
        required
        id="username"
        variant="filled"
        name="username"
        placeholder="username"
        onChange={handleChange}
      />
      <TextField
        required
        id="password"
        variant="filled"
        name="password"
        placeholder="password"
        onChange={handleChange}
      />
      <Button
        sx={{
          bgcolor: "#d01110",
          color: "#000000",
          "&:hover": {
            color: "#d01110",
            bgcolor: "#A9A9A9",
          },
        }}
        onClick={handleSubmit}
      >
        Sign Up
      </Button>
    </Card>
  );
}
