import { StatusBar } from "expo-status-bar";
import { StyleSheet, View, Text } from "react-native";
import {
  NativeStackNavigationProp,
  NativeStackScreenProps,
} from "@react-navigation/native-stack";
import { StackScreens } from "../../App";
import { Button } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";
import axios, { AxiosError } from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect, useState } from "react";

export default function Profile({}: NativeStackScreenProps<
  StackScreens,
  "Profile"
>) {
  const navigation = useNavigation<NativeStackNavigationProp<StackScreens>>();
  const [name, setName] = useState<string | null>();

  useEffect(() => {
    AsyncStorage.getItem("my-key").then((res) => {
      setName(res);
    });
  }, []);

  const handleSubmit = async () => {
    try {
      const results = await axios.post(
        `${process.env.EXPO_PUBLIC_API_ROOT}/auth/logout`,
        null,
        { withCredentials: true }
      );
      const status = results.data.success;
      if (status === true) {
        await AsyncStorage.removeItem("my-key");
        navigation.navigate("Home");
      }
    } catch (error) {
      const e = error as AxiosError;
      console.log(e);
    }
  };
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Text>hello {name}</Text>
      <Button mode="contained" onPress={handleSubmit}>
        Logout
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
