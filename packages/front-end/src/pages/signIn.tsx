import axios from "axios";
import Card from "@mui/material/Card";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { useState } from "react";
import { useRouter } from "next/navigation";
interface User {
  username: string;
  password: string;
}

export default function Login() {
  const [values, setValues] = useState<User>();
  const router = useRouter();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    const name = e.target.name;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleClick = async () => {
    const results = await axios.post(
      "http://localhost:50000/auth/login",
      {
        username: values.username,
        password: values.password,
      },
      { withCredentials: true }
    );
    alert(results.data.message);

    router.push("/");
  };

  return (
    <Card
      sx={{
        bgcolor: "transparent",
        width: "40vh",
        height: "25vh",
        display: "flex",
        overflow: "none",
        flexDirection: "column",
        justifyContent: "space-between",
        padding: "4vh",
      }}
    >
      <div>Hello Login Page</div>
      <TextField
        required
        id="username"
        variant="filled"
        name="username"
        placeholder="username"
        onChange={handleChange}
      />
      <TextField
        required
        id="password"
        variant="filled"
        name="password"
        placeholder="password"
        onChange={handleChange}
      />
      <Button
        sx={{
          bgcolor: "#d01110",
          color: "#000000",
          "&:hover": {
            color: "#d01110",
            bgcolor: "#A9A9A9",
          },
        }}
        onClick={handleClick}
      >
        Log in
      </Button>
    </Card>
  );
}
