const config = {
  http: {
    host: process.env.EXPRESS_HOST || "localhost",
    port: parseInt(process.env.EXPRESS_PORT, 10) || 50000,
  },
};

export default config;
